package spring.boot.thread.synchronization;

import spring.boot.thread.handler.NumberHandler;

public class CountNumberThread2 extends Thread {
    NumberHandler t;

    public CountNumberThread2(NumberHandler t) {
        this.t = t;
    }

    public void run() {
        t.printTable(100);
    }
}
