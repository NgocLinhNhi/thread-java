package spring.boot.thread.synchronization;

import spring.boot.thread.handler.NumberHandler;

public class CountNumberThread1 extends Thread {
    NumberHandler t;

    public CountNumberThread1(NumberHandler t) {
        this.t = t;
    }

    public void run() {
        t.printTable(5);
    }
}
