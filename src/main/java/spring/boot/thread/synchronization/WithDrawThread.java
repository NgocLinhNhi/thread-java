package spring.boot.thread.synchronization;

import spring.boot.thread.handler.CustomerBankHandler;

public class WithDrawThread extends Thread {

    String name;
    Thread thread;

    public WithDrawThread(String threadName) {
        start(threadName);
    }

    public void start(String threadName) {
        this.name = threadName;
        this.thread = new Thread(this, name);
        this.thread.setPriority(Thread.NORM_PRIORITY);
        this.thread.start();
    }


    @Override
    public void run() {
        try {
            handlerWithDraw();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void handlerWithDraw() throws InterruptedException {
        CustomerBankHandler.getInstance().withdraw(15000);
        System.out.println("Balance after withdraw: " + CustomerBankHandler.getInstance().getBalance());
    }
}
