package spring.boot.thread.synchronization;

import spring.boot.thread.handler.CustomerBankHandler;

public class DepositThread extends Thread {

    String name;
    Thread thread;

    public DepositThread(String threadName) {
        start(threadName);
    }

    public void start(String threadName) {
        this.name = threadName;
        this.thread = new Thread(this, name);
        this.thread.setPriority(Thread.NORM_PRIORITY);
        this.thread.start();
    }


    @Override
    public void run() {
        handlerDeposit();
    }

    public void handlerDeposit() {
        CustomerBankHandler.getInstance().deposit(10000);
        System.out.println("Balance after Deposit: " + CustomerBankHandler.getInstance().getBalance());
    }
}
