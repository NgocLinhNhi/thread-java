package spring.boot.thread.handler;

import spring.boot.thread.entity.Product;

public class ProductExecutorHandler {

    public void handlerProduct(Product product) {
        System.out.println("Product Name:" + product.getProductName());
    }
}
