package spring.boot.thread.handler;

public class CustomerBankHandler {
    private int balance = 10000;
    public boolean result = false;

    public static CustomerBankHandler INSTANCE;

    public static CustomerBankHandler getInstance() {
        if (INSTANCE == null) INSTANCE = new CustomerBankHandler();
        return INSTANCE;
    }

    public synchronized void deposit(int amount) {
        System.out.println("Starting Deposit...");
        this.balance += amount;
        result = true;
        //notify();
        System.out.println("Finished Deposit!");
    }

    public synchronized void withdraw(int amount) throws InterruptedException {
        System.out.println("Starting Withdraw...");
        if (this.balance < amount) {
            System.out.println("Your Account not enough money !!! please deposit...");
            result = false;
            //wait();
            return;
        }
        this.balance -= amount;
        System.out.println("Withdraw has finished!");
        result = true;
    }

    public synchronized int getBalance() {
        return balance;
    }
}
