package spring.boot.thread.handler;

public class ProductHandler {

    public static ProductHandler INSTANCE;

    //khai báo singleton
    public static ProductHandler getInstance() {
        if (INSTANCE == null) INSTANCE = new ProductHandler();
        return INSTANCE;
    }

    public void handleProduct(String threadName, Thread thread) {
        System.out.println("This is Product running");
        for (int i = 3; i > 0; i--) {
            System.out.println(threadName + ": Love Ninh " + i);
            try {
                thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
