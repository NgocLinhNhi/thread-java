package spring.boot.thread.handler;

public class ProducerHandler {

    public static ProducerHandler INSTANCE;

    public static ProducerHandler getInstance() {
        if (INSTANCE == null) INSTANCE = new ProducerHandler();
        return INSTANCE;
    }

    public void handlerProducer() {
        System.out.println("Producer Thread running");
    }
}
