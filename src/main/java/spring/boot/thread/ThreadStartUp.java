package spring.boot.thread;

import spring.boot.thread.classicthread.ProducerThread;
import spring.boot.thread.classicthread.ProductThread;

public class ThreadStartUp {

    public static void main(String[] args) {
        new ProducerThread("Producer Thread");
        try {
            Thread.sleep(3000);
            new ProductThread("Product Thread");
        } catch (InterruptedException e) {
            System.out.println("Main thread Interrupted");
        }
        System.out.println("Main thread exiting.");
    }
}
