package spring.boot.thread.classicthread;

import spring.boot.thread.handler.ProducerHandler;

public class ProducerThread extends Thread {
    String name;
    Thread thread;

    public ProducerThread(String threadName) {
        start(threadName);
    }

    public void start(String threadName) {
        this.name = threadName;
        this.thread = new Thread(this, name);
        this.thread.start();
    }

    @Override
    public void run() {
        handlerProducer();
    }

    public void handlerProducer() {
        ProducerHandler.getInstance().handlerProducer();
    }
}
