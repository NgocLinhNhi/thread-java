package spring.boot.thread.classicthread;

import spring.boot.thread.handler.ProductHandler;

public class ProductThread extends Thread {
    protected String name;
    protected Thread thread;

    public ProductThread(String threadName) {
        start(threadName);
    }

    public void start(String threadName) {
        this.name = threadName;
        this.thread = new Thread(this, name);
        this.thread.start();
    }

    @Override
    public void run() {
        handlerProgam();
    }

    public void handlerProgam() {
        ProductHandler.getInstance().handleProduct(name, thread);
    }
}
