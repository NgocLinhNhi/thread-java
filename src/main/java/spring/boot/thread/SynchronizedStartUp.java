package spring.boot.thread;

import spring.boot.thread.handler.CustomerBankHandler;
import spring.boot.thread.synchronization.DepositThread;
import spring.boot.thread.synchronization.WithDrawThread;


public class SynchronizedStartUp {
//    public static void main(String args[]) {
//        NumberHandler obj = new NumberHandler();
//        CountNumberThread1 t1 = new CountNumberThread1(obj);
//        CountNumberThread2 t2 = new CountNumberThread2(obj);
//        t1.start();
//        t2.start();
//    }

    public static void main(String args[]) {
        if (!CustomerBankHandler.getInstance().result) new DepositThread("deposit");
        new WithDrawThread("Withdraw");
    }
}
