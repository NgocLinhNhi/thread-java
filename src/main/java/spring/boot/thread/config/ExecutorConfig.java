package spring.boot.thread.config;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExecutorConfig {
    public static int threadPoolSize = 2;
    public static int threadPoolSizeLv2 = 10;
}
