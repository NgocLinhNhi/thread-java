package spring.boot.thread;

import spring.boot.thread.runable.ProducerRunnable;
import spring.boot.thread.runable.ProductRunnable;

public class RunnableStartUp {

    public static void main(String[] args) {
        new ProducerRunnable("ProducerThread");
        new ProductRunnable("ProductThread");

        System.out.println("Main thread exiting.");
    }
}
