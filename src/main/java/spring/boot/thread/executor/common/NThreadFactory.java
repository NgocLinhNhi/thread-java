package spring.boot.thread.executor.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class NThreadFactory implements ThreadFactory {

    private int poolId;
    protected int priority;
    protected String prefix;
    protected boolean daemon;
    protected String poolName;
    protected String threadPrefix;
    protected ThreadGroup threadGroup;
    protected AtomicInteger threadCounter = new AtomicInteger();

    Logger logger = LoggerFactory.getLogger(NThreadFactory.class);

    private static final AtomicInteger POOL_COUNTER = new AtomicInteger();

    public NThreadFactory(String threadName) {
        this.poolId = POOL_COUNTER.incrementAndGet(); // Tự tăng cho Pool
        this.poolName = threadName;
        this.daemon = false; // là Thread daemon -> Độ ưu tiên thấp -> chạy ngầm trên nền ứng dụng => ko ảnh hưởng Thread khác !
        this.prefix = "bo";
        this.priority = Thread.NORM_PRIORITY;// độ ưu tiên của Thread
        this.threadGroup = getSystemThreadGroup();
        this.poolName = getFullPoolName();
        this.threadPrefix = poolName + '-' + poolId + '-';
    }

    protected ThreadGroup getSystemThreadGroup() {
        return getSecurityManager() == null ? Thread.currentThread().getThreadGroup() : getSecurityManager().getThreadGroup();
    }

    protected SecurityManager getSecurityManager() {
        return System.getSecurityManager();
    }

    @Override
    public Thread newThread(Runnable runnable) {
        Thread thread = createThread(runnable, getThreadName());
        setUpThread(thread);
        return thread;
    }

    protected Thread createThread(Runnable runnable, String name) {
        return new Thread(runnable, name);
    }

    protected void setUpThread(Thread thread) {
        try {
            trySetUpThread(thread);
        } catch (Exception e) {
            logger.warn("can not setup processor {}", thread.getName(), e);
        }
    }

    protected void trySetUpThread(Thread thread) {
        if (thread.isDaemon()) {
            if (!daemon)
                thread.setDaemon(false);
        } else {
            if (daemon)
                thread.setDaemon(true);
        }
        if (thread.getPriority() != priority)
            thread.setPriority(priority);
    }

    protected String getThreadName() {
        return threadPrefix + threadCounter.incrementAndGet();
    }

    protected String getFullPoolName() {
        return prefix + "-" + poolName;
    }
}
