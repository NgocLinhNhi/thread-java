package spring.boot.thread.executor.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

public abstract class ThreadPoolLoop {

    Logger logger = LoggerFactory.getLogger(ThreadPoolLoop.class);

    protected volatile boolean active;
    protected final int threadPoolSize;
    protected final ExecutorService executorService;


    public ThreadPoolLoop(int threadPoolSize) {
        this(threadPoolSize, null);
    }

    public ThreadPoolLoop(int threadPoolSize, String threadNameSuffix) {
        this.threadPoolSize = threadPoolSize;
        ThreadFactory threadFactory = new NThreadFactory(
                getThreadName() +
                        (threadNameSuffix != null ? "-" + threadNameSuffix : "")
        );
        this.executorService = newExecutorService(threadPoolSize, threadFactory);
    }

    protected ExecutorService newExecutorService(int threadPoolSize, ThreadFactory threadFactory) {
        return Executors.newFixedThreadPool(threadPoolSize, threadFactory);
    }

    public final synchronized void start() {
        if (active) {
            logger.info("thread loop has started");
            return;
        }
        this.preStart();
        this.active = true;
        for (int i = 0; i < threadPoolSize; ++i)
            executorService.execute(() -> loop());
    }

    protected void preStart() {
        // Trước khi start của Thread cho làm gì thì làm ở đây
    }

    protected final void loop() {
        while (active) {
            try {
                handleLoopOne();
            } catch (Exception e) {
                logger.error("process exception", e);
            }
        }
    }

    protected abstract void handleLoopOne() throws Exception;

    //Gọi stop khi muốn stop Thread
    public void stop() {
        this.active = false;
    }

    protected abstract String getThreadName();

}
