package spring.boot.thread.executor.common;

import spring.boot.thread.entity.Product;

import java.util.ArrayList;
import java.util.List;

public class ProductCommon {

    public List<Product> getListProduct() {
        List<Product> lstProduct = new ArrayList<>();
        Product product = new Product();
        product.setAddress("NHA TRANG");
        product.setAge(30);
        product.setProductName("Viet");

        Product product1 = new Product();
        product1.setAddress("HA NOI");
        product1.setAge(30);
        product1.setProductName("NINH");

        lstProduct.add(product);
        lstProduct.add(product1);
        return lstProduct;
    }
}
