package spring.boot.thread.executor.task;

import spring.boot.thread.config.ExecutorConfig;
import spring.boot.thread.entity.Product;
import spring.boot.thread.executor.common.NThreadFactory;
import spring.boot.thread.executor.common.ProductCommon;
import spring.boot.thread.handler.ProductExecutorHandler;

import java.util.List;
import java.util.concurrent.*;

public class ExecutorFixedThreadPool extends ExecutorConfig {

    public static ExecutorFixedThreadPool INSTANCE;
    private ExecutorService executorService;
    private ExecutorService executorServiceFactory;

    public static ExecutorFixedThreadPool getInstance() {
        if (INSTANCE == null) INSTANCE = new ExecutorFixedThreadPool();
        return INSTANCE;
    }

    public void configExecutorService() {
        executorService = Executors.newFixedThreadPool(threadPoolSize);
    }

    public void configExecutorMultipleService(ThreadFactory threadFactory) {
        executorServiceFactory = Executors.newFixedThreadPool(threadPoolSize, threadFactory);
    }

    public void executeService() throws InterruptedException {
        configExecutorService();
        List<Product> listProduct = getListProduct();
        final CountDownLatch latch = new CountDownLatch(listProduct.size());
        handlerExecutorThread(listProduct);
        //latch.await();
        executorService.shutdown();// Không cho threadpool nhận thêm nhiệm vụ nào nữa
        boolean finished = executorService.awaitTermination(1, TimeUnit.DAYS);

        if (finished) System.out.println("Finished Executor");
        else System.out.println("Generate report take more than one day. Stop");
    }

    public void multipleExecutorService(String id) {
        ThreadFactory threadFactory = new NThreadFactory("jms-endpoint-" + id);
        configExecutorMultipleService(threadFactory);
        List<Product> listProduct = getListProduct();
        for (int i = 0; i < threadPoolSize; ++i) {
            listProduct.stream().forEach(product ->
                    executorServiceFactory.execute(() -> handleProduct(product))
            );
        }
        //tức là sẽ có 2 thread (threadFactory) đọc cái listProduct và in ra
    }

    public void handlerExecutorThread(List<Product> listProduct) {
        /**
         *
         *  use Executor with stream-foreach
         *
         * Mỗi 1 data foreach ra sẽ giao cho 1 thread thực hiện xử lý
         * threadPoolSize = 5 ở đây
         */
        listProduct.stream().forEach(this::accept);

        //Cách code cổ điển cho Foreach
//        for (Product p : listProduct) {
//            executorService.execute(new Runnable() {
//                @Override
//                public void run() {
//                    handleProduct(p);
//                }
//            });
//        }
    }

    public void handleProduct(Product product) {
        ProductExecutorHandler productExecutorHandler = new ProductExecutorHandler();
        productExecutorHandler.handlerProduct(product);
    }

    public List<Product> getListProduct() {
        ProductCommon productCommon = new ProductCommon();
        return productCommon.getListProduct();
    }

    private void accept(Product product) {
        executorService.execute(() -> handleProduct(product));
    }
}
