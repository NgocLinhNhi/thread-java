package spring.boot.thread.executor.task;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spring.boot.thread.config.ExecutorConfig;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ExecutorThreadFactoryNonCommon extends ExecutorConfig {
    Logger logger = LoggerFactory.getLogger(ExecutorThreadFactoryNonCommon.class);
    private static ExecutorService executorService;

    /**
     * Khai báo singleton
     */
    private static ExecutorThreadFactoryNonCommon INSTANCE = new ExecutorThreadFactoryNonCommon();

    public static ExecutorThreadFactoryNonCommon getInstance() {
        return INSTANCE;
    }

    public void innerClassRunnable() {
        Runnable run = () -> {
            productHandler();
        };
        getInstance().getExecutor().submit(run);
    }

    public void productHandler() {
        logger.debug("Executor Thread");
        System.out.println("Love NINH");
    }

    public static synchronized ExecutorService getExecutor() {
        if (executorService == null) {
            BasicThreadFactory factory;

            factory = new BasicThreadFactory.Builder()
                    .namingPattern("Test Executor" + "-%d").daemon(true)
                    .priority(Thread.MAX_PRIORITY).build();

            executorService = new ThreadPoolExecutor(threadPoolSize, threadPoolSize,
                    0L, TimeUnit.SECONDS, new LinkedBlockingQueue<>(), factory);
        }
        return executorService;
    }
}
