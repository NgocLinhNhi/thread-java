package spring.boot.thread.executor.task;

import spring.boot.thread.config.ExecutorConfig;
import spring.boot.thread.entity.Product;
import spring.boot.thread.executor.common.ProductCommon;
import spring.boot.thread.handler.ProductExecutorHandler;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ExecutorSingleThread extends ExecutorConfig {

    public static ExecutorSingleThread INSTANCE;
    private ExecutorService executorService;

    public static ExecutorSingleThread getInstance() {
        if (INSTANCE == null) INSTANCE = new ExecutorSingleThread();
        return INSTANCE;
    }

    public void generateReport() throws InterruptedException {
        executorService = Executors.newSingleThreadExecutor();
        List<Product> listProduct = getListProduct();

        listProduct.stream().forEach(product ->
                executorService.execute(() -> handleProduct(product))
        );

        executorService.shutdown();// Không cho threadpool nhận thêm nhiệm vụ nào nữa
        boolean finished = executorService.awaitTermination(1, TimeUnit.DAYS);

        if (finished) System.out.println("Finished Executor");
        else System.out.println("Generate report take more than one day. Stop");
    }

    public void handleProduct(Product product) {
        ProductExecutorHandler productExecutorHandler = new ProductExecutorHandler();
        productExecutorHandler.handlerProduct(product);
    }

    public List<Product> getListProduct() {
        ProductCommon productCommon = new ProductCommon();
        return productCommon.getListProduct();
    }
}
