package spring.boot.thread.callable;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class ExecuteProductCallable {

    ExecutorService executorService = Executors.newFixedThreadPool(5);
    List<Future<String>> listFuture = new ArrayList<>();

    public static ExecuteProductCallable INSTANCE;

    public static ExecuteProductCallable getInstance() {
        if (INSTANCE == null) INSTANCE = new ExecuteProductCallable();
        return INSTANCE;
    }

    public void start() throws ExecutionException, InterruptedException {
        productProcess();
    }


    private void productProcess() throws ExecutionException, InterruptedException {
        for (int i = 1; i <= 3; i++) {
            ProductCallable productCallable = new ProductCallable("Callable +" + i);
            Future<String> future = executorService.submit(productCallable);
            // Từng Future sẽ quản lý một Callable
            listFuture.add(future);
        }

        for (Future future : listFuture) {
            // Khi Thread nào kết thúc, get() của Future tương ứng sẽ trả về kết quả mà Callable return
            System.out.println(future.get() + " kết thúc");
        }
        executorService.shutdown();
    }

}
