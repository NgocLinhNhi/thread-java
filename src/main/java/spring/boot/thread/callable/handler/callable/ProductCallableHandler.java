package spring.boot.thread.callable.handler.callable;

public class ProductCallableHandler {

    public String handlerProductCallable(String name) {
        System.out.println(name + " Callable is running...");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return name;
    }
}
