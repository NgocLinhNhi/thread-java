package spring.boot.thread.callable;

import lombok.Getter;
import lombok.Setter;
import spring.boot.thread.callable.handler.callable.ProductCallableHandler;

import java.util.concurrent.Callable;

@Getter
@Setter
public class ProductCallable implements Callable<String> {

    private String name;

    public ProductCallable(String name) {
        this.name = name;
    }

    @Override
    public String call() {
        return handlerProduct(this.name);
    }

    public String handlerProduct(String name) {
        ProductCallableHandler productCallableHandler = new ProductCallableHandler();
        return productCallableHandler.handlerProductCallable(name);
    }
}
