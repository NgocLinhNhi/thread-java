package spring.boot.thread.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Product {

    protected String productName;
    protected int age;
    protected String address;
}
