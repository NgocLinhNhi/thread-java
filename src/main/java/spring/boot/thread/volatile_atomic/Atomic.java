package spring.boot.thread.volatile_atomic;

import java.util.concurrent.atomic.AtomicBoolean;

public class Atomic {

    private AtomicBoolean active = new AtomicBoolean(false);

    private void prepare() {
        new Thread(() -> {
            System.out.println("application preparing ...");
            sleep(3);
            active.set(true);
        }).start();
    }

    public void start() {
        new Thread(() -> {
            while (!active.get()) ;
            System.out.println("application started");
        }).start();
    }

    private static void sleep(int second) {
        try {
            Thread.sleep(second * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Atomic example = new Atomic();
        example.prepare();
        example.start();
        sleep(10);
    }
}
