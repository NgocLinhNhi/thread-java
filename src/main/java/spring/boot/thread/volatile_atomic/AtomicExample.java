package spring.boot.thread.volatile_atomic;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class AtomicExample {

    private AtomicInteger count = new AtomicInteger(0);
    private final Map<Integer, Integer> map = new ConcurrentHashMap<>();

    public void start() {
        Thread[] threads = new Thread[10];
        for (int i = 0; i < threads.length; ++i) {
            threads[i] = new Thread(() -> {
                int value;
                while ((value = count.incrementAndGet()) <= 1000000) {
                    map.put(value, value);
                }
            });
        }
        for (Thread thread : threads) {
            thread.start();
        }
    }

    //Atomic read 1.000.000 và trả về đủ 1000.000 records => Atomic dùng để đồng bộ dữ liệu tốt
    public static void main(String[] args) throws Exception {
        AtomicExample example = new AtomicExample();
        example.start();
        Thread.sleep(3000);
        System.out.println(example.map.size());
    }
}