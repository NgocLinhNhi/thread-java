package spring.boot.thread.volatile_atomic;

public class Volatile {

    private volatile boolean active;

    private void prepare() {
        new Thread(() -> {
            System.out.println("application preparing ...");
            sleep(3);
            active = true;
        }).start();
    }

    public void start() {
        new Thread(() -> {
            while (!active) ;
            System.out.println("application started");
        }).start();
    }

    private static void sleep(int second) {
        try {
            Thread.sleep(second * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //active biến active không có kiểu volatile thì không bao h start - thread có thể nhận được value = true
    //=> Luôn luôn trong vòng lặp while
    //Nên chỉ dùng volatile để làm điều kiện thông báo thôi .
    public static void main(String[] args) {
        Volatile example = new Volatile();
        example.prepare();
        example.start();
        sleep(10);
    }
}