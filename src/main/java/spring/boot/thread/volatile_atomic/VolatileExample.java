package spring.boot.thread.volatile_atomic;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class VolatileExample {

    private volatile int count;
    private final Map<Integer, Integer> map = new ConcurrentHashMap<>();

    public void start() {
        Thread[] threads = new Thread[10];
        for (int i = 0; i < threads.length; ++i) {
            threads[i] = new Thread(() -> {
                while (count <= 1000000) {
                    ++count;
                    map.put(count, count);
                }
            });
        }

        for (Thread thread : threads) {
            thread.start();
        }
    }

    //count = 1.000.000  ma in ra chỉ được có co 980.513 => bị mất gần 20k values => Volatile không dùng để đồng bộ dữ liệu được.
    //Nên chỉ dùng volatile để làm điều kiện thông báo thôi . không dùng để tính toán +- được
    public static void main(String[] args) throws Exception {
        VolatileExample example = new VolatileExample();
        example.start();
        Thread.sleep(3000);
        System.out.println(example.map.size());
    }
}