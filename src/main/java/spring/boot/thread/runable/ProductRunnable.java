package spring.boot.thread.runable;

import lombok.Getter;
import lombok.Setter;
import spring.boot.thread.handler.ProductHandler;

@Getter
@Setter
public class ProductRunnable implements Runnable {
    protected String name;
    protected Thread thread;

    public ProductRunnable(String threadName) {
        start(threadName);
    }

    public void start(String threadName) {
        this.name = threadName;
        this.thread = new Thread(this, name);
        this.thread.setPriority(Thread.MAX_PRIORITY);
        this.thread.start();
    }

    @Override
    public void run() {
        handleProduct();
    }

    public void handleProduct() {
        ProductHandler.getInstance().handleProduct(name, thread);
    }
}
