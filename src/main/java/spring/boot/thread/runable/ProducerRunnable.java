package spring.boot.thread.runable;

import spring.boot.thread.handler.ProducerHandler;

public class ProducerRunnable implements Runnable {
    String name;
    Thread thread;

    public ProducerRunnable(String threadName) {
        start(threadName);
    }

    public void start(String threadName) {
        this.name = threadName;
        this.thread = new Thread(this, name);
        this.thread.setPriority(Thread.MIN_PRIORITY);
        this.thread.start();
    }

    @Override
    public void run() {
        handleProducer();
    }

    public void handleProducer() {
        ProducerHandler.getInstance().handlerProducer();
    }
}
