package spring.boot.thread;

import spring.boot.thread.callable.ExecuteProductCallable;
import spring.boot.thread.executor.task.ExecutorFixedThreadPool;

import java.util.concurrent.ExecutionException;

public class ExecutorStartUp {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        //ExecutorThreadFactoryNonCommon.getInstance().innerClassRunnable();
        // ExecutorCachedThreadPool.getInstance().generateReport();
        //ExecutorFixedThreadPool.getInstance().executeService();
        // ExecutorSingleThread.getInstance().generateReport();
        ExecuteProductCallable.getInstance().start();

        //ExecutorFixedThreadPool.getInstance().multipleExecutorService("ExecutorThreadPool");
    }
}
