package spring.boot.thread.event_loop;

public class TestNonBlockingEventLoop {

    public static void main(String[] args) {
        NonBlockingEventLoop eventLoop = new NonBlockingEventLoop();
        Runnable updateViewEvent = new Runnable() {
            @Override
            public void run() {
                System.out.println("update state");
                eventLoop.addEvent(this);
            }
        };
        eventLoop.addEvent(updateViewEvent);
        eventLoop.onUpdate(() -> System.out.println("Update view"));
        eventLoop.start();
    }
}
