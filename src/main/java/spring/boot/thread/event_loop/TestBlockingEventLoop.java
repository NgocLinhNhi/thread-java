package spring.boot.thread.event_loop;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class TestBlockingEventLoop {
    public static void main(String[] args) {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        BlockingEventLoop eventLoop = new BlockingEventLoop();
        scheduler.scheduleAtFixedRate(() -> eventLoop.addEvent(TestBlockingEventLoop::handle), 0, 3, TimeUnit.SECONDS);
        eventLoop.start();
    }

    private static void handle() {
        System.out.println("event's processing");
    }
}
